from django.utils.translation import gettext_lazy as _
from django.conf import settings

from oscar.apps.customer.forms import EmailUserCreationForm

from django import forms

class RegistrationForm(EmailUserCreationForm):
    zipcode = forms.CharField(label=_('Zipcode'))

    def clean_zipcode(self):
        zipcode = self.cleaned_data['zipcode']
        if str(zipcode) not in settings.LOOMCLEANER_AVAILABILITY_REGIONS :
            raise forms.ValidationError(
                _("We’re not quite in your area!"))
        return zipcode