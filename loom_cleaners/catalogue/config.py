from oscar.apps.catalogue import config


class CatalogueConfig(config.CatalogueConfig):
    name = 'loom_cleaners.catalogue'

