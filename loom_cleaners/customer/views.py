from oscar.apps.customer.views import AccountAuthView as AuthView
from .forms import RegistrationForm


class AccountAuthView(AuthView):
    registration_form_class = RegistrationForm
