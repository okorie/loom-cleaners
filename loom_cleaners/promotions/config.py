from oscar.apps.promotions import config


class PromotionsConfig(config.PromotionsConfig):
    name = 'loom_cleaners.promotions'
