from oscar.apps.checkout import config


class CheckoutConfig(config.CheckoutConfig):
    name = 'loom_cleaners.checkout'
