from oscar.apps.promotions.views import HomeView as CoreHomeView
from loom_cleaners.catalogue.models import Product

class HomeView(CoreHomeView):
    
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        products = {
            "products": Product.objects.filter(parent=None).order_by('-date_created')
        }
        context.update(products)

        return context