from django import template

from oscar.core.loading import feature_hidden

register = template.Library()


@register.simple_tag
def reverse_discount(discounted_amount):
    return round(int(discounted_amount) / (1 - .45), 2)
