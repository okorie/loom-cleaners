from oscar.apps.promotions.app import PromotionsApplication as CorePromotionsApplication
from .views import HomeView

class PromotionsApplication(CorePromotionsApplication):
    extra_view = HomeView

application = PromotionsApplication()